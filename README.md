# NYC Schools Code Challenge

This is a mini app that shows lists of High Schools in New York City.
The app is designed with an implementation of an appstore inspired view in mind.
The animation is not yet implemented due to time constraint; however the code is written in a way
to implement the appstore-like animation.

## Getting Started
Download or Clone the app
```
git clone https://gitlab.com/dbronola/20190710-diannebronola-nycschools.git
```
You can test the app in your local machine with the following prerequisites:
* Xcode 10.2
* Swift 5

## Features
* MVVM
* Custom Views
* Unit Tests

## ScreenShots

![School List](/screenshots/list.png)
![School Detail](/screenshots/detail.png)
![Offline Error](/screenshots/offline.png)
