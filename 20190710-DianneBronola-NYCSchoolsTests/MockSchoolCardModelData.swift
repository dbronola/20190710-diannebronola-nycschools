//
//  MockSchoolCardModelData.swift
//  20190710-DianneBronola-NYCSchoolsTests
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
@testable import _0190710_DianneBronola_NYCSchools


public struct MockSchoolCardModelData {
    
    static let sc = NYCSchoolInfo(dbn: "01", overview_paragraph: "lorem ipsum first", school_name: "aes", school_email: nil, phone_number: "034-121-1122", website: "haha.hahu.com")
    static let sat = NYCSchoolSATScore(dbn: "01", num_of_sat_test_takers: "lorem ipsum second", sat_critical_reading_avg_score: "357", sat_math_avg_score: "895", sat_writing_avg_score: "121", school_name: "aes")
    static let firstSample = SchoolViewModel(schoolInfo: sc, satInfo: sat)
    
    static let sc1 = NYCSchoolInfo(dbn: "02", overview_paragraph: "lorem ipsum second", school_name: "dces", school_email: nil, phone_number: "650-991-6278", website: nil)
    static let sat1 = NYCSchoolSATScore(dbn: "02", num_of_sat_test_takers: "thousand", sat_critical_reading_avg_score: nil, sat_math_avg_score: "895", sat_writing_avg_score: "121", school_name: "dces")
    static let secondSample = SchoolViewModel(schoolInfo: sc1, satInfo: sat1)
    
    static let sc2 = NYCSchoolInfo(dbn: "03", overview_paragraph: "lorem ipsum second", school_name: "sact", school_email: nil, phone_number: "721-121-0891", website: "haha.hahu.com")
    static let sat2 = NYCSchoolSATScore(dbn: "03", num_of_sat_test_takers: "million", sat_critical_reading_avg_score: "357", sat_math_avg_score: nil, sat_writing_avg_score: "121", school_name: "sact")
    static let thirdSample = SchoolViewModel(schoolInfo: sc2, satInfo: sat2)
    
    static let mockList: [SchoolViewModel] = [firstSample, secondSample, thirdSample]
    
}
