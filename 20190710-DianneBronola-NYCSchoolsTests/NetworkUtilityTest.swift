//
//  NetworkUtilityTest.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import XCTest
@testable import _0190710_DianneBronola_NYCSchools

class NetworkUtilityTests: XCTestCase {
    
    var subject: NetworkUtility!
    
    override func setUp() {
        super.setUp()
        subject = NetworkUtility()
        
    }
    
    override func tearDown() {
        subject = nil
        super.tearDown()
    }
    
    func test_CreateDataTask() {
        let url = URL(string: "https://data.cityofnewyork.us/resource/97mf-9njv.json")!
        let successPromise = expectation(description: "success")
        
        subject.createDataTask(url: url) { data, success in
            XCTAssertNotNil(data, "data is not nil")
            XCTAssertTrue(success, "this should be successful")
            successPromise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_highSchoolDirectoryRequest() {
        let promise = expectation(description: "highSchoolDirectoryRequest sucessful")
        subject.highSchoolDirectoryRequest() { schoolList, success in
            guard let schoolList = schoolList else {
                XCTFail("should return schoolList")
                return
            }
            guard (success != false) else {
                XCTFail("request failed")
                return
            }
            promise.fulfill()
            XCTAssertEqual(schoolList.count, 440)
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_highSchoolSATScoreRequest() {
        let promise = expectation(description: "test_highSchoolSATScoreRequest sucessful")
        subject.highSchoolSATScoreRequest() { satScore, success in
            guard let satScore = satScore else {
                XCTFail("should return satScores")
                return
            }
            guard (success != false) else {
                XCTFail("request failed")
                return
            }
            promise.fulfill()
            XCTAssertEqual(satScore.count, 478)
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_dataForNYCSchoolList() {
        let schoolListData = "[{\"dbn\": \"21\", \"overview_paragraph\": \"art school\", \"school_name\": \"school for goths\", \"website\": \"www.nycshoolgoths.com\"}, {\"dbn\": \"22\", \"overview_paragraph\": \"sport school\", \"school_name\": \"school for jocks\", \"website\": \"www.nycshooljocks.com\"}]"
        let data = schoolListData.data(using: .utf8)
        var schoolItemsList = [NYCSchoolInfo]()
        let promise = expectation(description: "data downloaded")
        subject.dataForNYCSchoolList(data: data!, success: true) { schoolListData, success  in
            guard (success != false) else {
                return
            }
            
            guard let schoolData = schoolListData else {
                return
            }
            schoolItemsList = schoolData
            promise.fulfill()
        }
        XCTAssertEqual(schoolItemsList.count, 2)
        XCTAssertEqual(schoolItemsList[0].dbn, "21")
        XCTAssertEqual(schoolItemsList[1].school_name, "school for jocks")
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_dataForNYCSchoolSatScores() {
        let satScoresData = "[{\"dbn\": \"12\", \"num_of_sat_test_takers\": \"23\", \"sat_critical_reading_avg_score\": \"245\", \"sat_math_avg_score\": \"300\", \"sat_writing_avg_score\": \"100\", \"school_name\": \"hogwarts\"}]"
        let data = satScoresData.data(using: .utf8)
        var satScoreItems = [NYCSchoolSATScore]()
        let promise = expectation(description: "data downloaded")
        
        subject.dataForNYCSchoolSatScores(data: data!, success: true) { satScores, success in
            guard (success != false) else {
                return
            }
            
            guard let satScoreList = satScores else {
                return
            }
            
            satScoreItems = satScoreList
            promise.fulfill()
        }
        XCTAssertEqual(satScoreItems.count, 1)
        XCTAssertEqual(satScoreItems[0].sat_math_avg_score, "300")
        XCTAssertEqual(satScoreItems[0].sat_critical_reading_avg_score, "245")
        waitForExpectations(timeout: 5, handler: nil)
    }
}
