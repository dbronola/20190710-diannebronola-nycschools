//
//  NYCSchoolListViewControllerTest.swift
//  20190710-DianneBronola-NYCSchoolsTests
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import XCTest
@testable import _0190710_DianneBronola_NYCSchools

class NYCSchoolListViewControllerTest: XCTestCase {
    
    var subject: NYCSchoolListViewController!

    override func setUp() {
        subject = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolListViewController") as? NYCSchoolListViewController
        subject.didFinishDownloadingData(schoolViewModelList: MockSchoolCardModelData.mockList)
    }

    func testViewSetUpOnLoad() {
        subject.viewDidLoad()
        
        XCTAssertNotNil(subject.tableView)
        XCTAssertNotNil(subject.viewModel.delegate)
    }

}
