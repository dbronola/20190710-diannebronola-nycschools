//
//  UITableView+Extensions.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit

// Inspired by https://gist.github.com/gonzalezreal/92507b53d2b1e267d49a
extension UITableView {
    
    func registerCell<Cell: UITableViewCell>(_ cellClass: Cell.Type) {
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass))
    }
    
    func dequeueReusableCell<Cell: UITableViewCell>(forIndexPath indexPath: IndexPath) -> Cell {
        let identifier = String(describing: Cell.self)
        guard let cell = self.dequeueReusableCell(withIdentifier: identifier,
                                                  for: indexPath) as? Cell else {
                                                    fatalError("Error for cell id: \(identifier) at \(indexPath))")
        }
        return cell
    }
}
