//
//  UILabel+Extensions.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by MLS Discovery on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit


extension UILabel {
    
    func configureAttributedText(font: UIFont, color: UIColor, text: String) {
        let attributes: [NSAttributedString.Key: Any] = [
            NSAttributedString.Key.font :font,
            NSAttributedString.Key.foregroundColor: color
        ]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        self.attributedText = attributedString
    }
    
    func configureSchoolTitle(text: String) {
        self.text = text
        self.font = UIFont.systemFont(ofSize: 28.0, weight: .bold)
        self.textAlignment = .left
        self.numberOfLines = 0
        self.lineBreakMode = .byTruncatingTail
    }

}
