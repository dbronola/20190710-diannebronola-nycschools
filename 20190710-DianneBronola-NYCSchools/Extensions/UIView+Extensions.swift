//
//  UIView+Extensions.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import UIKit

extension UIView {
    
    var safeTopAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.topAnchor
        }
        return self.topAnchor
    }
    
    var safeLeadingAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *){
            return self.safeAreaLayoutGuide.leadingAnchor
        }
        return self.leadingAnchor
    }
    
    var safeTrailingAnchor: NSLayoutXAxisAnchor {
        if #available(iOS 11.0, *){
            return self.safeAreaLayoutGuide.trailingAnchor
        }
        return self.trailingAnchor
    }
    
    var safeBottomAnchor: NSLayoutYAxisAnchor {
        if #available(iOS 11.0, *) {
            return self.safeAreaLayoutGuide.bottomAnchor
        }
        return self.bottomAnchor
    }
    
    func pinToParent(view: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.safeLeadingAnchor).isActive = true
        self.trailingAnchor.constraint(equalTo: view.safeTrailingAnchor).isActive = true
        self.topAnchor.constraint(equalTo: view.safeTopAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: view.safeBottomAnchor).isActive = true
    }
    
    func pinToParentWithMargin(view: UIView, top: CGFloat = 0, bottom: CGFloat = 0, left: CGFloat = 0, right: CGFloat = 0) {
        translatesAutoresizingMaskIntoConstraints = false
        self.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: left).isActive = true
        self.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: right * -1).isActive = true
        self.topAnchor.constraint(equalTo: view.topAnchor, constant: top).isActive = true
        self.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: bottom * -1).isActive = true
    }
    
    func pinTopLeft(toView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.topAnchor.constraint(equalTo: toView.topAnchor, constant: 20.0).isActive = true
        self.leadingAnchor.constraint(equalTo: toView.leadingAnchor, constant: 20.0).isActive = true
        self.trailingAnchor.constraint(lessThanOrEqualTo: toView.trailingAnchor, constant: -20).isActive = true
    }
    
    func pinBottomLeft(toView: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: toView.bottomAnchor, constant: -20).isActive = true
        self.leadingAnchor.constraint(equalTo: toView.leadingAnchor, constant: 20.0).isActive = true
        self.trailingAnchor.constraint(lessThanOrEqualTo: toView.trailingAnchor, constant: -20).isActive = true
    }
}
