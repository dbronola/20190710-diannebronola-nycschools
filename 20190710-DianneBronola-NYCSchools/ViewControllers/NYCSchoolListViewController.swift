//
//  NYCSchoolListViewController.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import UIKit

class NYCSchoolListViewController: UIViewController {
    
    let tableView = UITableView()
    let viewModel = NYCSchoolListViewModel()
    var schoolViewModelData = [SchoolViewModel]()
    
    var networkErrorView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
        getSchoolData()
        
    }
    
    private func getSchoolData() {
        viewModel.delegate = self
        viewModel.createBothRequest()
    }
    
    private func setUpViews() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.registerCell(SchoolCardTableViewCell.self)
        
        view.addSubview(tableView)
        tableView.pinToParent(view: view)
    }

}

extension NYCSchoolListViewController: DataHolderDelegate {
    
    func didFinishDownloadingData(schoolViewModelList: [SchoolViewModel]) {
        schoolViewModelData = schoolViewModelList
        tableView.reloadData()
    }
    
    func didEncounterError() {
        DispatchQueue.main.async {
            self.networkErrorView = self.viewModel.createNetworkErrorView()
            if let nv = self.networkErrorView {
                self.view.addSubview(nv)
                nv.pinToParent(view: self.view)
            }
        }
    }
}

extension NYCSchoolListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolViewModelData.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cardCell = tableView.dequeueReusableCell(forIndexPath: indexPath) as SchoolCardTableViewCell
        let cardViewModel = schoolViewModelData[indexPath.row]
        
        //making sure that no two cell views in one cell
        guard let cellView = cardCell.cellView else {
            let schoolCardView = SchoolCardView(viewModel: cardViewModel)
            cardCell.cellView = schoolCardView
            return cardCell
        }
        
        cellView.viewModel = cardViewModel
        cardCell.clipsToBounds = false
        cardCell.contentView.clipsToBounds = false
        
        cardCell.layer.masksToBounds = false
        cardCell.contentView.layer.masksToBounds = false
        cardCell.cellView?.layer.masksToBounds = false
        return cardCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cardViewModel = schoolViewModelData[indexPath.row]
        
        guard let detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NYCSchoolDetailViewController") as? NYCSchoolDetailViewController else {
            fatalError("Can't instantiate NYCSchoolDetailViewController")
        }
        detailViewController.schoolViewModel = cardViewModel
        present(detailViewController, animated: true, completion: nil)
        
        //To wake up the UI, Apple issue with cells with selectionStyle = .none
        CFRunLoopWakeUp(CFRunLoopGetCurrent())
    }
}
