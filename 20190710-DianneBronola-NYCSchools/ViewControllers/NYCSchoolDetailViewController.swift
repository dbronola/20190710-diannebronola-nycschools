//
//  NYCSchoolDetailViewController.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import UIKit
class NYCSchoolDetailViewController: UIViewController {
    
    var schoolViewModel: SchoolViewModel?
    var viewModel = NYCSchoolDetailViewModel()
    
    
    @IBOutlet weak var containerCardView: UIView!
    @IBOutlet weak var schoolDescriptionLabel: UILabel!
    @IBOutlet weak var satScoreStack: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let schoolVM = schoolViewModel {
            let cardView = SchoolCardView(viewModel: schoolVM)
            containerCardView?.addSubview(cardView)
            cardView.pinToParent(view: containerCardView)
            
            viewModel.addConfigureSATScoreView(stackView: satScoreStack, model: schoolVM)
        }
        
        configureSchoolDescription()
        
        
    }
    
    func configureSchoolDescription() {
        schoolDescriptionLabel.configureAttributedText(font: UIFont.systemFont(ofSize: 20, weight: .regular), color: UIColor.gray, text: schoolViewModel?.schoolInfo.overview_paragraph ?? Constants.missingData)
        schoolDescriptionLabel.textAlignment = .left
        schoolDescriptionLabel.numberOfLines = 0
    }
    
    
    @IBAction func close(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
