//
//  NYCSchoolDetailViewModel.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/12/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit

class NYCSchoolDetailViewModel: NSObject {
    
   
    func addConfigureSATScoreView(stackView: UIStackView, model: SchoolViewModel) {
        let a = addConfigureSATItemScoreView(score: model.satInfo?.sat_math_avg_score ?? Constants.missingData, subject: Constants.math)
        let b = addConfigureSATItemScoreView(score: model.satInfo?.sat_writing_avg_score ?? Constants.missingData, subject: Constants.writing)
        let c = addConfigureSATItemScoreView(score: model.satInfo?.sat_critical_reading_avg_score ?? Constants.missingData, subject: Constants.reading)
        
        stackView.addArrangedSubview(a)
        stackView.addArrangedSubview(b)
        stackView.addArrangedSubview(c)
        
        stackView.spacing = 10.0
    }
    
    
    
    private func addConfigureSATItemScoreView(score: String, subject: String) -> UIView {
        let boardView = UIView()
        boardView.backgroundColor = UIColor(red:0.42, green:0.69, blue:0.30, alpha:1.0)
        boardView.layer.borderWidth = 5
        boardView.layer.borderColor = UIColor.darkGray.cgColor
        
        let horizontalStack = UIStackView()
        let scoreLabel = UILabel()
        let subjectLabel = UILabel()
        
        guard let font = UIFont(name: "Chalkduster", size: 15.0) else {
            return UIView()
        }
        let color = UIColor.white
        
        scoreLabel.configureAttributedText(font: font, color: color, text: score)
        subjectLabel.configureAttributedText(font: font, color: color, text: subject)
        
        horizontalStack.axis = .horizontal
        horizontalStack.addArrangedSubview(subjectLabel)
        horizontalStack.addArrangedSubview(scoreLabel)
        
        boardView.addSubview(horizontalStack)
        
        horizontalStack.pinToParentWithMargin(view: boardView, left: 20.0, right: 20.0)
        horizontalStack.heightAnchor.constraint(equalToConstant: 100.00).isActive = true
        return boardView
    }
    
}
