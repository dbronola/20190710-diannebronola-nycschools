//
//  NYCSchoolListViewModel.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit

protocol DataHolderDelegate: class {
    func didFinishDownloadingData(schoolViewModelList: [SchoolViewModel])
    func didEncounterError()
}


class NYCSchoolListViewModel: NSObject {
    
    //This is the viewModel for InitialViewController, handles data download for data needed in the app
    
    var schoolViewModelList = [SchoolViewModel]()
    var schoolList = [NYCSchoolInfo]()
    var satScores = [NYCSchoolSATScore]()
    
    weak var delegate: DataHolderDelegate?
    var networkHandler: NetworkUtilityProtocol = NetworkUtility.networkUtil()
    
    // method to trigger the network call,
    // download in group to reserve resource, for more opportunities to add features using downloaded data
    // and notify the viewcontroller using delegate
    
    func createBothRequest() {
        
        let dispatchGroup = DispatchGroup()
        
        dispatchGroup.enter()
        networkHandler.highSchoolDirectoryRequest{ (schoolList, success) in
            
            guard (success != false) else {
                self.delegate?.didEncounterError()
                return
            }
            
            guard let list = schoolList else {
                self.delegate?.didEncounterError()
                return
            }
            
            self.schoolList = list
            dispatchGroup.leave()
        }
        
        dispatchGroup.enter()
        networkHandler.highSchoolSATScoreRequest{ (satScore, success) in
            guard (success != false) else {
                self.delegate?.didEncounterError()
                return
            }
            
            guard let scores = satScore else {
                self.delegate?.didEncounterError()
                return
            }
            self.satScores = scores
            dispatchGroup.leave()
        }
        
        dispatchGroup.notify(queue: .main){
            print("Download Data Successful")
            
            self.schoolViewModelList = self.schoolList.map {
                let schoolDbn = $0.dbn
                if let i = self.satScores.firstIndex(where: {$0.dbn == schoolDbn}) {
                    return SchoolViewModel(schoolInfo: $0, satInfo: self.satScores[i])
                }
                return SchoolViewModel(schoolInfo: $0, satInfo: nil)
            }
            self.delegate?.didFinishDownloadingData(schoolViewModelList: self.schoolViewModelList)
        }
    }
    
    func createNetworkErrorView() -> UIView {
        let backGroundView = UIView()
        let offLineStackView = UIStackView()
        
        let errorTitle = UILabel()
        let errorFirstMessage = UILabel()
        let errorSecondMessage = UILabel()
        
        errorTitle.configureAttributedText(font: UIFont.systemFont(ofSize: 15, weight: .bold), color: .black, text: "No internet connection")
        errorFirstMessage.configureAttributedText(font: UIFont.systemFont(ofSize: 12, weight: .regular), color: .darkGray, text: "Check your mobile data or Wi-Fi or get a")
        errorSecondMessage.configureAttributedText(font: UIFont.systemFont(ofSize: 12, weight: .regular), color: .darkGray, text: "notification when your connection comes back")
        
        offLineStackView.axis = .vertical
        offLineStackView.addArrangedSubview(errorTitle)
        offLineStackView.addArrangedSubview(errorFirstMessage)
        offLineStackView.addArrangedSubview(errorSecondMessage)
        
        backGroundView.addSubview(offLineStackView)
        offLineStackView.translatesAutoresizingMaskIntoConstraints = false
        offLineStackView.centerXAnchor.constraint(equalTo: backGroundView.centerXAnchor).isActive = true
        offLineStackView.centerYAnchor.constraint(equalTo: backGroundView.centerYAnchor).isActive = true
        
        return backGroundView
    }
}
