//
//  NYCSchoolData.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation


public struct NYCSchoolInfo: Decodable {
    let dbn: String?
    let overview_paragraph: String?
    let school_name: String?
    let school_email: String?
    let phone_number: String?
    let website: String?
}

public struct NYCSchoolSATScore: Decodable {
    let dbn: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
    let school_name: String?
}

public class NYCSchoolDataList {
    
    var NYCSchoolList = [NYCSchoolInfo]()
    var NYCSchoolSATScoreList = [NYCSchoolSATScore]()
    
    init(schoolList: [NYCSchoolInfo], satScores: [NYCSchoolSATScore]){
        NYCSchoolList = schoolList
        NYCSchoolSATScoreList = satScores
    }
    
}
