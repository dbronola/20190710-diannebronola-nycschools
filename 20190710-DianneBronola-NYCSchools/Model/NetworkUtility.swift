//
//  NetworkUtility.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/11/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation

public protocol NetworkUtilityProtocol {
    func highSchoolDirectoryRequest(completion: @escaping (_ schoolList: [NYCSchoolInfo]?, _ success: Bool)-> Void)
    func highSchoolSATScoreRequest(completion: @escaping(_ satScore: [NYCSchoolSATScore]?, _ success: Bool)-> Void)
}

class NetworkUtility: NSObject, NetworkUtilityProtocol {
    
    class func networkUtil() -> NetworkUtility {
        struct Singleton {
            static var networkUtil = NetworkUtility()
        }
        return Singleton.networkUtil
    }
    
    // MARK: url constants
    static let highSchoolListURL = "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    static let highSchoolSATScoresURL = "https://data.cityofnewyork.us/resource/734v-jeq5.json"
    
    // MARK: Network Call
    
    // common function to call the api endpoint and get data
    func createDataTask(url: URL, completionHandler: @escaping (_ data: Data?, _ success: Bool) -> Void){
        
        let request = URLRequest(url: url)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            func logError(error: String){
                completionHandler(nil, false)
                print(error)
            }
            
            guard (error == nil) else {
                logError(error: "There was an error with the request: \(String(describing: error))")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                logError(error: "The request returned a status code other than 2xx!")
                return
            }
            
            guard let data = data else {
                logError(error: "No data was returned by the request!")
                return
            }
            
            completionHandler(data, true)
        }
        task.resume()
    }
    
    // Main API to call Request for HighSchool Directory
    func highSchoolDirectoryRequest(completion: @escaping (_ schoolList: [NYCSchoolInfo]?, _ success: Bool)-> Void) {
        
        if let url = URL(string: NetworkUtility.highSchoolListURL) {
            createDataTask(url: url) { (data, success) in
                guard let data = data else {
                    completion(nil, false)
                    return
                }
                self.dataForNYCSchoolList(data: data, success: success){ (schoolList, success) in
                    completion(schoolList, true)
                }
            }
        }
    }
    // Main API to call Request for SAT Scores
    func highSchoolSATScoreRequest(completion: @escaping(_ satScore: [NYCSchoolSATScore]?, _ success: Bool)-> Void) {
        
        if let url = URL(string: NetworkUtility.highSchoolSATScoresURL) {
            createDataTask(url: url) { (data, success) in
                guard let data = data else {
                    completion(nil, false)
                    return
                }
                self.dataForNYCSchoolSatScores(data: data, success: success) { (satScores, success) in
                    completion(satScores, true)
                }
            }
        }
    }
    
    
    
    // Parsing JSON data
    
    func dataForNYCSchoolList(data: Data, success: Bool, completion: @escaping (_ schoolList: [NYCSchoolInfo]?, _ success: Bool) -> Void) {
        var schoolItems = [NYCSchoolInfo]()
        
        do {
            try schoolItems = JSONDecoder().decode([NYCSchoolInfo].self, from: data)
        } catch {
            completion(nil, false)
            print ("Error parsing JSON")
        }
        
        completion(schoolItems, true)
    }
    
    func dataForNYCSchoolSatScores(data: Data, success: Bool, completion: @escaping (_ satScores: [NYCSchoolSATScore]?, _ success: Bool) -> Void) {
        var satScoreInfo = [NYCSchoolSATScore]()
        
        do {
            try satScoreInfo = JSONDecoder().decode([NYCSchoolSATScore].self, from: data)
        } catch {
            completion(nil, false)
            print ("Error parsing JSON")
        }
        
        completion(satScoreInfo, true)
    }
    
    
}
