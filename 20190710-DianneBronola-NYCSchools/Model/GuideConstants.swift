//
//  GuideConstants.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/12/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    static let missingData = "Data Not Found"
    static let math = "Math"
    static let writing = "Writing"
    static let reading = "Critical Reading"
}
