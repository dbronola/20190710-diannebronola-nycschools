//
//  SchoolViewModel.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import Foundation
import UIKit

enum CardViewMode {
    //This is the mode that tells when a card is used inside the detail VC
    //or in the cell of the table vc
    case full
    case card
}


class SchoolViewModel {
    var viewMode: CardViewMode = .card
    var schoolInfo: NYCSchoolInfo
    var satInfo: NYCSchoolSATScore?
    
    init(schoolInfo: NYCSchoolInfo, satInfo: NYCSchoolSATScore?) {
        self.schoolInfo = schoolInfo
        self.satInfo = satInfo
    }
    
}

