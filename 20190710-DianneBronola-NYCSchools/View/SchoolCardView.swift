//
//  SchoolCardView.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import UIKit

//The reasoning behind this view is
//the initial plan to implement a appStore-like animation feel
//given more time, I would add a transition delegate
//and add animation properties when a card cell is selected

class SchoolCardView: UIView {

    
    var viewModel: SchoolViewModel
    
    let containerView = UIView()
    let shadowView = UIView()
    
    private let schoolNameLabel = UILabel()
    private let schoolWebsiteButton = UIButton()
    private let schoolEmailButton = UIButton()
    private let schoolPhoneButton = UIButton()
    
    var leftConstraint:NSLayoutConstraint = NSLayoutConstraint()
    var rightConstraint:NSLayoutConstraint = NSLayoutConstraint()
    var topConstraint:NSLayoutConstraint = NSLayoutConstraint()
    var bottomConstraint:NSLayoutConstraint = NSLayoutConstraint()
    
    private var gradientBackground: CAGradientLayer?
    
    init(viewModel: SchoolViewModel) {
        self.viewModel = viewModel
        super.init(frame: .zero)
        
        leftConstraint = NSLayoutConstraint(item: containerView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0)
        rightConstraint = NSLayoutConstraint(item: containerView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: 0)
        topConstraint = NSLayoutConstraint(item: containerView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: 0)
        bottomConstraint = NSLayoutConstraint(item: containerView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: 0)
        
        setUpViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setContainerColor()
    }
    
    //MARK: Shadows and Card Content, needed for Card Like experience
    
    func addShadow() {
        shadowView.layer.cornerRadius = 20
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOpacity = 0.2
        shadowView.layer.shadowRadius = 10
        shadowView.layer.shadowOffset = CGSize(width: -1, height: 2)
    }
    
    func removeShadow() {
        shadowView.layer.shadowColor = UIColor.clear.cgColor
        shadowView.layer.shadowOpacity = 0
        shadowView.layer.shadowRadius = 0
        shadowView.layer.shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func updateLayout(for viewMode: CardViewMode) {
        switch viewMode {
        case .card:
            updateContainerToCardView()
        case .full:
            updateContainerToFullScreen()
        }
    }
    
    func updateContainerToCardView() {
        leftConstraint.constant = 20
        rightConstraint.constant = -20
        topConstraint.constant = 15
        bottomConstraint.constant = -15
        
        addShadow()
        
        containerView.layer.cornerRadius = 20
        containerView.layer.masksToBounds = true
    }
    
    func updateContainerToFullScreen() {
        leftConstraint.constant = 0
        rightConstraint.constant = 0
        topConstraint.constant = 0
        bottomConstraint.constant = 0
        
        removeShadow()
        
        containerView.layer.cornerRadius = 0
        containerView.layer.masksToBounds = true
    }
    
    // MARK: Main function for setting up views
    
    func setUpViews() {
        backgroundColor = .clear
        addSubview(shadowView)
        shadowView.backgroundColor = .white
        setContainerColor()
        addSubview(containerView)
       
        
        if viewModel.viewMode == .card {
            updateContainerToCardView()
        } else {
            updateContainerToFullScreen()
        }
        
        shadowView.pinToParent(view: containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        addConstraints([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
    
        addTitleLabel(schoolName: viewModel.schoolInfo.school_name ?? Constants.missingData)
        addSubLabels(url: viewModel.schoolInfo.website ?? Constants.missingData,
                     email: viewModel.schoolInfo.school_email ?? Constants.missingData,
                     phone: viewModel.schoolInfo.phone_number ?? Constants.missingData)
    }
    
    func setContainerColor(){
        gradientBackground?.removeFromSuperlayer()
        gradientBackground = CAGradientLayer()
        let blue = UIColor(red:0.04, green:0.47, blue:0.87, alpha:1.0)
        let purple = UIColor(red:0.42, green:0.54, blue:0.80, alpha:1.0)
        gradientBackground?.colors = [blue.cgColor, purple.cgColor]
        gradientBackground?.startPoint = CGPoint(x: 0, y:0)
        gradientBackground?.endPoint = CGPoint(x: 1, y: 1)
        
        gradientBackground?.frame = containerView.bounds
        containerView.layer.insertSublayer(gradientBackground ?? CAGradientLayer(), at: 0)
    }
    
    //MARK: Configure Labels
    
    func addTitleLabel(schoolName: String) {
        containerView.addSubview(schoolNameLabel)
        
        schoolNameLabel.configureSchoolTitle(text: schoolName)
        
        schoolNameLabel.pinTopLeft(toView: containerView)
    }
    
    func addSubLabels(url: String, email: String, phone: String) {
        let stackView = UIStackView()
        containerView.addSubview(stackView)
        
        stackView.pinBottomLeft(toView: containerView)
        stackView.axis = .vertical
        
        schoolWebsiteButton.setTitle(url, for: .normal)
        schoolEmailButton.setTitle(email, for: .normal)
        schoolPhoneButton.setTitle(phone, for: .normal)
        
        schoolWebsiteButton.addTarget(self, action: #selector(openUrl), for: .touchUpInside)
        
        stackView.alignment = .leading
        stackView.addArrangedSubview(schoolWebsiteButton)
        stackView.addArrangedSubview(schoolEmailButton)
        stackView.addArrangedSubview(schoolPhoneButton)
    }
    
    @objc func openUrl() {
        if let url = URL(string: viewModel.schoolInfo.website ?? Constants.missingData) {
            UIApplication.shared.open(url)
        }
    }

}
