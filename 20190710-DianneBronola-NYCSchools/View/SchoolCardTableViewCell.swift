//
//  SchoolCardTableViewCell.swift
//  20190710-DianneBronola-NYCSchools
//
//  Created by Dianne Bronola on 7/10/19.
//  Copyright © 2019 Dianne Bronola. All rights reserved.
//

import UIKit

class SchoolCardTableViewCell: UITableViewCell {
    
    var cellView: SchoolCardView? {
        //why is there an extra check here?
        didSet {
            guard cellView != nil else { return }
            setUpViews()
        }
    }
    
    //do I need an initializer?

    private func setUpViews() {
        guard let cellView = cellView else { return }
        addSubview(cellView)
        cellView.pinToParent(view: self)
    }

}
